package com.kwin.flink.configuration;
import com.kwin.flink.FlinkStarter;
import com.kwin.flink.sink.CustomSinkFunction;
import com.kwin.flink.sink.FlinkConsumerListener;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author kwin
 * @Date 2022/7/25 16:33
 **/
@Configuration
public class FlinkConf {

    @Bean(name = "flinkProperty")
    @ConfigurationProperties("flink")
    @ConditionalOnMissingBean
    public FlinkProperty flinkProperty() {
        return new FlinkProperty();
    }

    @Bean
    @ConditionalOnMissingBean
    public SinkFunction sinkFunction(List<FlinkConsumerListener> flinkConsumerListenerList) {
        return new CustomSinkFunction(flinkConsumerListenerList);
    }

    @Bean
    @ConditionalOnMissingBean
    public FlinkStarter flinkStarter(FlinkProperty flinkProperty, SinkFunction sinkFunction) {
        FlinkStarter flinkStarter = new FlinkStarter(flinkProperty, sinkFunction);
        flinkStarter.init();
        return flinkStarter;
    }

}
