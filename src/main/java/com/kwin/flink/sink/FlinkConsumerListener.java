package com.kwin.flink.sink;

import java.io.Serializable;

/**
 * @author kwin
 * @Date 2022/7/25 15:13
 **/
public interface FlinkConsumerListener<T> extends Serializable {

    String getDBName();
    String getTable();

    void insert(T data);

    void update(T srcData, T destData);

    void delete(T data);
}
